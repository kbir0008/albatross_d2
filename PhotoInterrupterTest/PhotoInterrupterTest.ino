#define thres 500      // Threshold value for sensor
int count = 0;       // Counter for sensor value changes
bool isOpen = false; // Flag to track open/closed state
bool displayCount = false; // Flag to enable/disable count display

void setup() {
  Serial.begin(9600); // Initialize serial communication
}

void loop() {
  int sensorValue = analogRead(A0); // Read the sensor value
  
  if ((sensorValue > thres) && isOpen) {
    // Blocked
    isOpen = false;
    count++;
    if (displayCount) {
      Serial.print("Count incremented: ");
      Serial.println(count);
    }
  } else if ((sensorValue < thres) && !isOpen) {
    // Open
    isOpen = true;
    count++;
    if (displayCount) {
      Serial.print("Count incremented: ");
      Serial.println(count);
    }
  }
  
  // Check for serial commands
  if (Serial.available() > 0) {
    char command = Serial.read();
    
    // Command to reset count
    if (command == 'R' || command == 'r') {
      count = 0;
      Serial.println("Count reset.");
    }
    
    // Command to get count
    if (command == 'C' || command == 'c') {
      Serial.print("Count: ");
      Serial.println(count);
    }
    
    // Command to enable/disable count display
    if (command == 'D' || command == 'd') {
      displayCount = !displayCount;
      if (displayCount) {
        Serial.println("Count display enabled.");
      } else {
        Serial.println("Count display disabled.");
      }
    }
  }
  
  // Add your other code here (if any)
}
