/*
    Albatross Payload release Mechanism
    Official Version
    By Katja Biro for Drone 2 MEC2402 Special teams project
    Last modified 24/5/23
    Final Release Rev 1.1
*/

// Include custom motorclass
#include <Arduino.h>
#include "L298N.h"

// Show debug output?
#define PRINT_DEBUG 1
#define NOTIFY_COUNTER_INTERVAL 50

// Define pins for motor control
#define PIN_RV_MAIN_SPOOL 4
#define PIN_EN_MAIN_SPOOL 3
#define PIN_RV_RELEASE 5
#define PIN_EN_RELEASE 6

// Define pin used for signalling from the Albatross
#define PIN_ALB_SIG 8

// Define the rotation sensor pin
#define PIN_ROT_SENS A1

// Define the rotation sensor theshold
#define SENSOR_THRESH 500   // This is the boundary between open and closed (up to 1024)

// Set timeout for spool deployment
#define SPOOL_TIMEOUT 20000       // How long after receiving signal do we wait?

// Create motor objects
Motor motorTetherLowerer(PIN_RV_MAIN_SPOOL,PIN_EN_MAIN_SPOOL, 255);
Motor motorTetherRelease(PIN_RV_RELEASE,PIN_EN_RELEASE, 255);

void setup() {
  pinMode(PIN_ALB_SIG, INPUT);
  #if (PRINT_DEBUG == 1)
    // If we want to display debugging information, set the compiler
    // flag PRINT_DEBUG to 1. This starts the serial communication
    Serial.begin(9600);
    Serial.println("Program started with debugging");
  #endif

}

void loop() {
  #if (PRINT_DEBUG == 1)
  // Send SOL message
  Serial.println("Waiting for signal from albatross");
  #endif

  // Wait until we get the ready to deploy signal
  while (digitalRead(PIN_ALB_SIG) == 0) {
    delay(100);
  }

  #if (PRINT_DEBUG == 1)
    Serial.println("Start Signal Recieved");
  #endif

  // Define relevant variables used to monitor states
  long endTime = millis() + SPOOL_TIMEOUT;              // Define the end time (current time + 30000ms)
  long counterTimeout = millis() + SPOOL_TIMEOUT+1000;  // Define the end time (current time + 30000ms)
  int counterMainSpool = 0;                             // Define how many time the rotation sensor 2 pulsed (8 per rotation)
  bool isOpenMainSpool = false;                         // Define if the rotation sensor 2 is (not) blocked
  bool didSpin = false;                                 // Store if we did lower payload via gravity
  
  #if PRINT_DEBUG 
    // Debug Variables
    int lastcountMainSpool2 = 0;
    bool notfied_Deploy = false;
  #endif
  
  #if (PRINT_DEBUG == 1)
    Serial.println("Waiting to extention to begin");
  #endif
  
  motorTetherLowerer.setReverse(false); // Set motor to run in reverse to provide resistance
  delay(20);  // Give the relay a sec to switch over
  motorTetherLowerer.setPWM(30);  // Set the resistance power to 30/255

  // Loop this until there are no more rotations or we timed out.
  while ((counterTimeout > millis()) & (endTime > millis())) {
    int sensorValue_2 = analogRead(PIN_ROT_SENS); // Get reading from rotation sensor
    
    // Interpret the rotation sensor
    if ((sensorValue_2 > SENSOR_THRESH) && isOpenMainSpool) {
      isOpenMainSpool = false;
      counterMainSpool++;
      counterTimeout = millis() + 5000;
      #if PRINT_DEBUG
        if (didSpin & !notfied_Deploy) {
          Serial.println("Deploying...");
          notfied_Deploy = true;
        }
      #endif
      if (counterMainSpool > 8) {
        // We have recorded a full rotation, apply brake and extend timeout time
        didSpin = true;
        motorTetherLowerer.start();
        endTime = endTime + 30000;
      }
    } else if ((sensorValue_2 < SENSOR_THRESH) && !isOpenMainSpool) {
      isOpenMainSpool = true;
      counterMainSpool++;
      counterTimeout = millis() + 5000;
      #if PRINT_DEBUG
        if (didSpin & !notfied_Deploy) {
          Serial.println("Deploying...");
          notfied_Deploy = true;
        }
      #endif
      if (counterMainSpool > 8) {
        // We have recorded a full rotation, apply brake and extend timeout time
        didSpin = true;
        motorTetherLowerer.start();
        endTime = endTime + 30000;
      }
    }

    #if PRINT_DEBUG
      // Print debug information
      if ((lastcountMainSpool2 + NOTIFY_COUNTER_INTERVAL) == counterMainSpool) {
        Serial.print("2: ");
        Serial.println(counterMainSpool);
        lastcountMainSpool2 = counterMainSpool;
      }
    #endif
  }

  if (!didSpin) {
    // Timeout was reached. Loop back to start.
    #if (PRINT_DEBUG == 1)
      Serial.println("Deployment Timed Out!");
    #endif
    goto LABEL_DEPLOY_FAILED;
  }

  // Stop the brake
  motorTetherLowerer.stop();

  #if (PRINT_DEBUG == 1)
    Serial.println("Deploying Payload...");
  #endif
  delay(500);

  // Set both motors to PULL and set the release motor to full power
  motorTetherLowerer.setReverse(false);
  motorTetherRelease.setReverse(false);
  motorTetherRelease.setPWM(255);
  
  // Quickly pull via the release motor to drop the payload net
  long deployTimeout = millis() + 100; // Pull up for 100ms
  motorTetherRelease.start();
  while (deployTimeout > millis()) {}
  motorTetherRelease.stop();

  #if (PRINT_DEBUG == 1)
    Serial.println("Deployed");
  #endif

  delay(2000); // Cooloff delay

  #if PRINT_DEBUG 
    lastcountMainSpool2 = counterMainSpool;
  #endif


  endTime = millis() + SPOOL_TIMEOUT;

  // Set the reel-up motors to high power
  motorTetherLowerer.setPWM(200); // This is the retaction start power

  // Kickstart the pull-up
  long retractKickstartTimeout = millis() + 50; // Kick start time 50ms
  motorTetherLowerer.start();

  while (retractKickstartTimeout > millis()) {}

  // Reduce power but keep reeling in
  motorTetherLowerer.setPWM(75); // This is the retaction power
  motorTetherLowerer.start();

  #if (PRINT_DEBUG == 1)
    Serial.println("Waiting for retraction...");
  #endif
  counterTimeout = millis() + SPOOL_TIMEOUT;
  // Loop until the holder is back in the Payload bay
  while ((counterMainSpool > 0) & (counterTimeout > millis()) & (endTime > millis())) {
    int sensorValue_2 = analogRead(PIN_ROT_SENS);
    if ((sensorValue_2 > SENSOR_THRESH) && isOpenMainSpool) {
      isOpenMainSpool = false;
      counterMainSpool--;
      counterTimeout = millis() + 2000;
    } else if ((sensorValue_2 < SENSOR_THRESH) && !isOpenMainSpool) {
      isOpenMainSpool = true;
      counterMainSpool--;
      counterTimeout = millis() + 2000;
    }
    #if PRINT_DEBUG
      if ((lastcountMainSpool2 - NOTIFY_COUNTER_INTERVAL) == counterMainSpool) {
        Serial.print("2: ");
        Serial.println(counterMainSpool);
        lastcountMainSpool2 = counterMainSpool;
      }
    #endif
  }

  #if (PRINT_DEBUG == 1)
    Serial.println("Done!");
  #endif

  // Stop both motors
  LABEL_DEPLOY_FAILED:
  motorTetherLowerer.stop();
  motorTetherRelease.stop();
}