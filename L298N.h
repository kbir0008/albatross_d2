// Custom driver class for motor driver L298N
// Katja Biro
// Last modified 2/5/23

#ifndef MOTOR_H
#define MOTOR_H

class Motor {
  private:
    // reversePin: the pin used to set the reverse direction
    // forwardPin: the pin used to set the forward direction
    // enablePin: the pin used to enable the motor
    // pwm: the initial PWM duty cycle (0-255)
    // rampTime: the ramp-up time in milliseconds
    bool reversed;
    int reversePin;
    int enablePin;
    int pwm;
    int rampTime;
    bool _isRunning;


  public:
    Motor(int reversePin, int enablePin, int pwm, int rampTime = 0);
    void setReverse(bool reverse);
    void setPWM(int pwm);
    void start();
    void stop();
    bool getReversed();
    int getPWM();
};

#endif // MOTOR_H
