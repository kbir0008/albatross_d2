// Custom driver class for motor driver L298N
// Katja Biro
// Last modified 2/5/23

#include "L298N.h"
#include <Arduino.h>

Motor::Motor(int reversePin, int enablePin, int pwm, int rampTime = 0) {
  this->reversed = false;
  this->reversePin = reversePin;
  this->enablePin = enablePin;
  this->pwm = pwm;
  this->rampTime = rampTime;
  this->_isRunning = false;
  
  pinMode(reversePin, OUTPUT);
  pinMode(enablePin, OUTPUT);
  digitalWrite(reversePin, LOW);
  digitalWrite(enablePin, LOW);
}

void Motor::setReverse(bool reverse) { // Default, reverse both
  this->reversed = reverse;
  digitalWrite(this->reversePin, reverse ? HIGH : LOW);
}

void Motor::setPWM(int pwm) {
  this->pwm = pwm;
  if (this->_isRunning) {
    analogWrite(enablePin, pwm);
  }
}

void Motor::start() {
  this->_isRunning = true;
  // Ramp up the motor gradually (if set)
  if (rampTime != 0) {
    for (int i = pwm/2; i <= pwm; i++) {
      analogWrite(enablePin, i);
      delay(rampTime / pwm); // add a delay to ramp up the PWM duty cycle gradually
    }
  } else if (rampTime == -1) { // If -1 set motor to half desired power and then jump to desired power after 500ms
    analogWrite(enablePin, pwm/2);
    delay(500);
    analogWrite(enablePin, pwm);
  } else { // If 0, set to desired power immediately
    analogWrite(enablePin, pwm);
  }
}

void Motor::stop() {
  this->_isRunning = false;
  digitalWrite(enablePin, LOW);
}

bool Motor::getReversed() {
  return this->reversed;
}

int Motor::getPWM() {
  return this->pwm;
}