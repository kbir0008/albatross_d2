# AlbatrossPayloadMCS
The code for operating the motors in D2s payload release mechanism.
Designed to run on an Arduino UNO using custom motor driver

## Layout
- `Albatross_Payload_Release.ino`
    Main Program. Controls the stepper motors
- `L298N.h`
    Header file containing the definitions in the motor class
- `L298N.cpp`
    Class file containing methods used in the motor class

## Operation
Pull the signal pin high then allow the payload to fall. The Uno should detect this and pull on the payload. When the payload reaches the bottom the central cable should be pulled to release carrier net. The main cables should then retract to pull up the payload net holder.